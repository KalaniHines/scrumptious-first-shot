#import model form from dj forms
from django.forms import ModelForm
#import recipe models from recipe
from recipes.models import Recipe




#create new class RF from model form
class RecipeForm(ModelForm):
    #create 'inner class' Meta
    class Meta:
        #create var model set to Recipe model
        model = Recipe
        #set fields
        fields = [
            'title',
            'picture',
            'description',
        ]
